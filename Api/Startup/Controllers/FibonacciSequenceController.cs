﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Startup.Controllers
{
    [Route("fibonacci-sequence")]
    [ApiController]
    public class FibonacciSequenceController : ControllerBase
    {
        private readonly IFibonacciNumberCalculator _fibonacciNumberCalculator;

        public FibonacciSequenceController(IFibonacciNumberCalculator fibonacciNumberCalculator)
        {
            _fibonacciNumberCalculator = fibonacciNumberCalculator;
        }

        [HttpGet("closest-sequence-number")]
        public async Task<IActionResult> FindClosestSequenceNumberAsync(int number)
        {
            return Ok(await _fibonacciNumberCalculator.FindClosestSequenceNumber(number));
        }

        [HttpGet("next-sequence-number")]
        public async Task<IActionResult> FindNextSequenceNumberAsync(int currentSequenceNumber)
        {
            int previousSequenceNumber;
            try
            {
                previousSequenceNumber = await _fibonacciNumberCalculator.FindPreviousSequenceNumber(currentSequenceNumber);
            }
            catch (Exception exception)
            {
                return BadRequest(exception.InnerException);
            }

            return Ok(await _fibonacciNumberCalculator.FindNextSequenceNumber(currentSequenceNumber, previousSequenceNumber));
        }
    }
}
