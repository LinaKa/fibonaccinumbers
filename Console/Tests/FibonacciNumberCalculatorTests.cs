using FibonacciNumbers;
using NUnit.Framework;
using System;
using System.Threading.Tasks;

namespace Tests
{
    public class FibonacciNumberCalculatorTests
    {
        private FibonacciNumberCalculator _sut;

        [SetUp]
        public void Setup()
        {
            _sut = new FibonacciNumberCalculator("");
        }

        [TestCase(0, 0)]
        [TestCase(1, 1)]
        [TestCase(22, 21)]
        [TestCase(30, 34)]
        [TestCase(165380141, 165580141)]
        public async Task should_return_correct_closest_sequence_number(int inputNumber, int expectedFibonacciNumber)
        {
            int result = await _sut.FindClosestSequenceNumber(inputNumber);

            Assert.That(result, Is.EqualTo(expectedFibonacciNumber));
        }

        [TestCase(0, 0)]
        [TestCase(1, 0)]
        [TestCase(21, 13)]
        [TestCase(55, 34)]
        public async Task should_return_correct_previous_sequence_number(int currentNumber, int expectedPreviousNumber)
        {
            int result = await _sut.FindPreviousSequenceNumber(currentNumber);

            Assert.That(result, Is.EqualTo(expectedPreviousNumber));
        }

        [Test]
        public void should_only_accept_sequence_number_when_calculating_previous()
        {
            Assert.ThrowsAsync<Exception>(async () => await _sut.FindPreviousSequenceNumber(7));
        }

        [TestCase(1, 1)]
        [TestCase(21, 34)]
        public async Task should_return_correct_next_sequence_number(int currentNumber,
            int expectedNextNumber)
        {
            int result = await  _sut.FindNextSequenceNumber(currentNumber);

            Assert.That(result, Is.EqualTo(expectedNextNumber));
        }
    }
}