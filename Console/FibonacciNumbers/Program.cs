﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FibonacciNumbers
{
    class Program
    {
        public static async Task Main(string[] args)
        {
            Console.WriteLine("Welcome to Fibonacci number calculator!");

            var listOfCalculators =
                new List<FibonacciNumberCalculator> {
                    new FibonacciNumberCalculator("node-1"),
                    new FibonacciNumberCalculator("node-2"),
                    new FibonacciNumberCalculator("node-3")
                };

            Console.WriteLine("Three nodes are ready for use");

            Random randomNumberGenerator = new Random();

            var fibonacciNumbersTasks = new List<Task<int>>();

            listOfCalculators.ForEach((FibonacciNumberCalculator x) => {
                fibonacciNumbersTasks.Add(x.FindClosestSequenceNumber(randomNumberGenerator.Next(int.MaxValue/2)));
            });

            var fibonacciNumbers = new List<int>();

            fibonacciNumbersTasks.ForEach(async x => fibonacciNumbers.Add(await x));

            int sequenceEnd = fibonacciNumbers.Max();
            int sequenceStart = fibonacciNumbers.Min();

            Console.WriteLine($"Calculators returned numbers {string.Join(";", fibonacciNumbers)}. " +
                $"Will now proceed to calculate sequence starting at {sequenceStart} and ending at {sequenceEnd}");

            int currentSequenceNumber = sequenceStart;
            var calculator = listOfCalculators.First();

            while (currentSequenceNumber < sequenceEnd)
            {
                int calculatedSequenceNumber = await calculator.FindNextSequenceNumber(currentSequenceNumber);

                Console.WriteLine(calculator.CalculatorName + "," + calculatedSequenceNumber);

                currentSequenceNumber = calculatedSequenceNumber;

                calculator = listOfCalculators
                    .OrderBy(x => Guid.NewGuid())
                    .First(x => x.CalculatorName != calculator.CalculatorName);
            };

            Console.WriteLine("\npress any key to exit the process...");
            Console.ReadKey();
        }
    }
}
