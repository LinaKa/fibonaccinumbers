﻿using System;
using System.Threading.Tasks;

namespace FibonacciNumbers
{
    public class FibonacciNumberCalculator
    {
        public string CalculatorName;

        public FibonacciNumberCalculator(string calculatorName)
        {
            CalculatorName = calculatorName; 
        }

        public async Task<int> FindClosestSequenceNumber(int number)
        {
            int prev = 0;
            int current = 1;

            while (current < number)
            {
                int sum = prev + current;
                prev = current;
                current = sum;
            }

            int diffFromLower = number - prev;
            int diffFromHigher = current - number;

            return diffFromLower > diffFromHigher ? current : prev;
        }

        public async Task<int> FindPreviousSequenceNumber(int sequenceNumber)
        {
            // this assumes that if you got a 1 its the first posible one in the sequence
            if (sequenceNumber == 0 || sequenceNumber == 1)
                return 0;

            int prev = 0;
            int current = 1;

            while (prev + current <= sequenceNumber)
            {
                int sum = prev + current;
                prev = current;
                current = sum;
            }

            if (current != sequenceNumber)
                throw new Exception("input number is not a valid fibonacci sequence number");

            return prev;
        }

        public async Task<int> FindNextSequenceNumber(int currentSequenceNumber)
        {
            int previousSequenceNumber = await FindPreviousSequenceNumber(currentSequenceNumber);
            int nextSequenceNumber = currentSequenceNumber + previousSequenceNumber;

            return nextSequenceNumber;
        }
    }
}
