﻿using System.Collections.Generic;

namespace Startup.Models
{
    public class SequenceViewModel
    {
        public int SequenceStartPoint { get; set; }
        public int SequenceEndPoint { get; set; }

        public IList<CalculationInformation> SequenceCalculationInformation { get; set; }
    }

    public class CalculationInformation
    {
        public string Machine { get; set; }
        public int Number { get; set; }
    }
}
