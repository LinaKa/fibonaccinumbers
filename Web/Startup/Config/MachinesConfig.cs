﻿using System.Collections.Generic;

namespace Startup.Config
{
    public class MachinesConfig
    {
        public List<Machine> Machines = new List<Machine>
        {
            new Machine{Name = "node1", Url = "http://localhost:5000"},
            new Machine{Name = "node2", Url = "http://localhost:5001"},
            new Machine{Name = "node3", Url = "http://localhost:5002"}
        };
    }

    public class Machine
    {
        public string Name { get; set; }
        public string Url { get; set; }
    }
}
