﻿using Microsoft.AspNetCore.Mvc;
using Startup.Config;
using Startup.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Startup.Controllers
{
    public class SequenceController : Controller
    {
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly MachinesConfig _machinesConfig;

        public SequenceController(IHttpClientFactory httpClientFactory, MachinesConfig machinesConfig)
        {
            _httpClientFactory = httpClientFactory;
            _machinesConfig = machinesConfig;
        }

        public async Task<IActionResult> Index()
        {
            var resultViewModel = new SequenceViewModel
            {
                SequenceCalculationInformation = new List<CalculationInformation>()
            };

            var listOfMachines =
                new List<HttpClientDo>();

            _machinesConfig.Machines.ForEach((machine) =>
            {
                listOfMachines.Add(new HttpClientDo
                {
                    Name = machine.Name,
                    HttpClient = _httpClientFactory.CreateClient(machine.Name)
                });
            });

            Random randomNumberGenerator = new Random();

            var fibonacciNumbers = new List<int>();

            foreach (var machine in listOfMachines)
            {
                int randomNumber = randomNumberGenerator.Next(int.MaxValue / 2);
                string closestNumberAsString =
                await machine.HttpClient.GetStringAsync($"fibonacci-sequence/closest-sequence-number?number={randomNumber}");

                fibonacciNumbers.Add(int.Parse(closestNumberAsString));
            }

            int sequenceEnd = fibonacciNumbers.Max();
            int sequenceStart = fibonacciNumbers.Min();

            resultViewModel.SequenceStartPoint = sequenceStart;
            resultViewModel.SequenceEndPoint = sequenceEnd;

            int currentSequenceNumber = sequenceStart;
            var machineInUse = listOfMachines.First();

            while (currentSequenceNumber < sequenceEnd)
            {
                string calculatedSequenceNumberAsString =
                    await machineInUse
                    .HttpClient
                    .GetStringAsync($"fibonacci-sequence/next-sequence-number?currentSequenceNumber={currentSequenceNumber}");

                int calculatedSequenceNumber = int.Parse(calculatedSequenceNumberAsString);

                if (calculatedSequenceNumber != sequenceEnd)
                    resultViewModel.SequenceCalculationInformation.Add(
                    new CalculationInformation
                    {
                        Number = calculatedSequenceNumber,
                        Machine = machineInUse.Name
                    });

                currentSequenceNumber = calculatedSequenceNumber;

                machineInUse = listOfMachines
                    .OrderBy(x => Guid.NewGuid())
                    .First(x => x.Name != machineInUse.Name);
            };

            return View(resultViewModel);
        }
    }
}
