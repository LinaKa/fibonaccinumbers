﻿using Microsoft.AspNetCore.Mvc;

namespace Startup.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
