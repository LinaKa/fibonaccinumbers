﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Startup.Config;
using System;

namespace Startup
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddHttpClient();

            var machinesConfig = new MachinesConfig();

            machinesConfig.Machines.ForEach((machine) => {
                services.AddHttpClient(machine.Name, c =>
                {
                    c.BaseAddress = new Uri(machine.Url);
                });
            });

            services.AddSingleton(machinesConfig);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
